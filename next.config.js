/** @type {import('next').NextConfig} */
const nextConfig = {
assetPrefix: process.env.NODE_ENV === 'production' ? '/nextjs-blog' : '',
images: {
      unoptimized: true,
    },
reactStrictMode: true,
swcMinify: true,
export: true,
output: 'export'
}

module.exports = nextConfig
